package kg.optima.reactive.dto;

public record Data(
        String number
) {
}
