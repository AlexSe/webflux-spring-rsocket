package kg.optima.reactive.client;

import io.rsocket.SocketAcceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.messaging.rsocket.RSocketStrategies;
import org.springframework.messaging.rsocket.annotation.support.RSocketMessageHandler;
import org.springframework.util.MimeTypeUtils;
import reactor.util.retry.Retry;

@Configuration
public class ClientConfiguration {

    @Value("${rsocket.server.port}")
    private Integer port;

    @Value("${rsocket.server.address}")
    private String host;

    // https://docs.spring.io/spring-framework/docs/current/reference/html/rsocket.html
    // https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html#rsocket
    @Bean
    public RSocketRequester rSocketRequester(RSocketStrategies rSocketStrategies) {

        // Для каждого нестандартного типа нужен encoder/decoder,
        //  т.к. по сетке летит девственный байт-массив
        // Для объектов достаточно Джексона
        RSocketStrategies strategies = RSocketStrategies.builder()
                .encoders(encoders -> {
                    encoders.add(0, new Jackson2JsonEncoder());
                })
                .decoders(decoders -> {
                    decoders.add(0, new Jackson2JsonDecoder());
                })
                .build();

        SocketAcceptor responder = RSocketMessageHandler
                .responder(strategies, new ClientHandler());

        return RSocketRequester.builder()
                .rsocketConnector(connector -> connector

                        // Будет ожидать подключения к серверу
                        // TODO:) Ведёт себя неадекватно при возникновении ошибок,
                        //  поэтому нужен exceptionHandler
                        .reconnect(Retry.indefinitely())

                        // Обработчик запросов ClientHandler() - можно валидировать метаданные,
                        //  по задумке используется для авторизции, а-ля некий фильтр Spring Security
                        // https://docs.spring.io/spring-framework/docs/current/reference/html/rsocket.html#rsocket-annot-responders-client
                        // По доке методы хендлера анотируются @ConnectMapping или @MessageMapping
                        .acceptor(responder)
                )
                .dataMimeType(MimeTypeUtils.APPLICATION_JSON)
                .rsocketStrategies(strategies)
                .tcp(host, port);
    }

}
