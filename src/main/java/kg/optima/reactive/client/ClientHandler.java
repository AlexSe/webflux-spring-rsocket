package kg.optima.reactive.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.rsocket.annotation.ConnectMapping;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class ClientHandler {

    // https://docs.spring.io/spring-framework/docs/current/reference/html/rsocket.html#rsocket-annot-connectmapping
    // Срабатывает в момент нового соединения (SETUP), для обмена метаданными, что-то вроде превичного хендшейка
    @ConnectMapping
    public void handle(@Headers Map<String, Object> metadata) {
        log.info("INIT SETUP");
        log.info("METADATA: {}", metadata.toString());
    }

}
