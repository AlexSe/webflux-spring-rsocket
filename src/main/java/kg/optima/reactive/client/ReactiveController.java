package kg.optima.reactive.client;

import kg.optima.reactive.dto.Data;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Service
@RestController
@RequestMapping
public class ReactiveController {

    @Autowired
    private RSocketRequester rSocketRequester;

    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Publisher<Data> stream(@RequestBody Data data) { // Входной параметр не имеет значения
        return rSocketRequester
                .route("stream")
                .data(data)
                .retrieveFlux(Data.class);
    }

    @GetMapping(value = "/requestResponse", produces = MediaType.APPLICATION_JSON_VALUE)
    public Publisher<Data> requestResponse(@RequestBody Data data) {
        return rSocketRequester
                .route("requestResponse")
                .data(data)
                .retrieveMono(Data.class);
    }

}
