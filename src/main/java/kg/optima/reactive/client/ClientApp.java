package kg.optima.reactive.client;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"kg.optima.reactive.client", "kg.optima.reactive.dto"})
@SpringBootApplication
public class ClientApp {

    public static void main(String[] args) {
        new SpringApplicationBuilder()
                .main(ClientApp.class)
                .sources(ClientApp.class, ClientConfiguration.class, ClientHandler.class, ReactiveController.class)
                .profiles("client")
                .run(args)
        ;
    }

}
