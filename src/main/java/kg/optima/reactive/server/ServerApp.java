package kg.optima.reactive.server;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"kg.optima.reactive.server", "kg.optima.reactive.dto"})
@SpringBootApplication
public class ServerApp {

    public static void main(String[] args) {
        new SpringApplicationBuilder()
                .main(ServerApp.class)
                .sources(ServerApp.class, ReactiveServer.class)
                .profiles("server")
                .run(args)
        ;
    }

}
