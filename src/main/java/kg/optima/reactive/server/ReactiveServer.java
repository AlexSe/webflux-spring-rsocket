package kg.optima.reactive.server;

import kg.optima.reactive.dto.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
public class ReactiveServer {

    private static final List<Data> dataList = new ArrayList<>();

    @PostConstruct
    public void init() {
        for (int i = 0; i < 100; i++) {
            dataList.add(new Data(Integer.toString(i)));
        }
    }

    // Работает на WebSocket
    @MessageMapping("stream")
    public Flux<Data> stream() {
        return Flux.just(dataList).flatMapIterable(data -> data.stream().toList());
    }

    // Режим request-response (точка-точка)
    @MessageMapping("requestResponse")
    public Data requestResponse(Data d) {
        log.info("REQUEST RESPONSE INVOKED!");
        return new Data("1");
    }

}
